//
//  MarlistViewModelTest.swift
//  NasaAppIosTests
//
//  Created by David Braga  on 22/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import XCTest
@testable import NasaAppIos
import RxSwift
import Swinject

class MarlistViewModelTest: XCTestCase {

    var viewModel : MarsListViewModel!
    var mockedRepo : MarsPhotoRepoMock!
    var disposeBag  = DisposeBag()
    
    override func setUp() {
        mockedRepo = MarsPhotoRepoMock()
        viewModel = MarsListViewModelImp(repo: mockedRepo, router: NasaNavigator(with: Container()))
    }
    
    
    func test_getMarsPhoto_returnRightData() {
        viewModel
            .loadMarsPhoto(sol: 2)
        
        viewModel.loadSubject.subscribe(onNext: { (marsPhoto) in
            XCTAssertEqual(self.mockedRepo.mockData!, marsPhoto)
        })
        
        
    }

}
