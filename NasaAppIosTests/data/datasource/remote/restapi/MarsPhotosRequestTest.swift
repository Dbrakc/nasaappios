//
//  MarsPhotosRequestTest.swift
//  NasaAppIosTests
//
//  Created by David Braga  on 22/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import XCTest
@testable import NasaAppIos

class MarsPhotosRequestTest: XCTestCase {
    
    let marsPhotoRequest = MarsPhotosRequest()

    func test_requestExist() {
        XCTAssertNotNil(marsPhotoRequest)
    }
    
    func test_request_returnRightRequest(){
        XCTAssertEqual(marsPhotoRequest.request(with: URL(string: Url.BASE.rawValue)!).description,URLRequest(url: URL(string:"")!).description)
    }

}
