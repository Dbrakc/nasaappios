//
//  MarsPhotoRepoImpTest.swift
//  NasaAppIosTests
//
//  Created by David Braga  on 22/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import XCTest
@testable import NasaAppIos
import RxSwift
import RxTest
class MarsPhotoRepoImpTest: XCTestCase {

    var repo : MarsPhotoRepoImp!
    var scheduler: TestScheduler!
    var mockedDataSouce : MarsPhotoDataSourceMock!
    var disposeBag  = DisposeBag()
    
    override func setUp() {
        mockedDataSouce = MarsPhotoDataSourceMock()
        repo = MarsPhotoRepoImp(with: mockedDataSouce)
        scheduler = TestScheduler(initialClock: 0)
    }
    

    func test_getMarsPhoto_returnRightData() {
        repo
            .getMarsPhoto()
            .subscribe(
                onNext: { (marsPhoto) in
                    XCTAssertEqual(self.mockedDataSouce.mockData!, marsPhoto)
            },
                onError: {_ in
                XCTAssertTrue(false)
            }).disposed(by: disposeBag)
        
    }
}
