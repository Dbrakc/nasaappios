//
//  AppDelegate.swift
//  NasaAppIos
//
//  Created by David Braga  on 17/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var container = Container()
    var router: NasaNavigator?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        router = NasaNavigator(with: container)
        registerContainers()
        let marsListVC = container.resolve(MarsListViewController.self)
        let navigatorVC = marsListVC?.wrappedInNavigation()
        navigatorVC?.navigationBar.isTranslucent = true
        if #available(iOS 11.0, *) {
            navigatorVC?.navigationBar.prefersLargeTitles = true
        }
        window?.rootViewController = navigatorVC
        window?.makeKeyAndVisible()
        return true
    }

}


