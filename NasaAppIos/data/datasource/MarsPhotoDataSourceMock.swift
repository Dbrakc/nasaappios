//
//  MarsPhotoDataSourceMock.swift
//  NasaAppIos
//
//  Created by David Braga  on 22/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
import RxSwift

class MarsPhotoDataSourceMock: MarsPhotoDataSource{
    var date = Date()
    var mockData : [MarsPhoto]?
    
    init(){
        mockData = (1...9).map{
            MarsPhoto(
                withId: $0,
                date: date,
                imageUrl: "image \($0)",
                rover: "rover  \($0)",
                cameraDetails: CameraDetails(
                withId: $0,
                name: "camera_name \($0)",
                roverId: $0,
                fullName: "full_name \($0)"
            )
        )
            
        }
    }
    
    func getMarsPhoto(sol: Int, page: Int) -> Observable<[MarsPhoto]> {
        return Observable.just(mockData!)
    }
    
    
}
