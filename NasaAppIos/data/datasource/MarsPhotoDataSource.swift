//
//  MarsPhotoDataSource.swift
//  NasaAppIos
//
//  Created by David Braga  on 19/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
import RxSwift

protocol MarsPhotoDataSource{
    var mockData: [MarsPhoto]? {get}
    func getMarsPhoto(sol: Int, page: Int) -> Observable<[MarsPhoto]>
}


