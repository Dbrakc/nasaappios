//
//  RemoteDataSource.swift
//  NasaAppIos
//
//  Created by David Braga  on 19/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
import RxSwift
struct RemoteDataSource : MarsPhotoDataSource {

     var mockData: [MarsPhoto]?
     let apiClient = APIClient()
    
    
    func getMarsPhoto(sol: Int = 2, page: Int = 1) -> Observable<[MarsPhoto]> {
        let apiRequest : APIRequest = MarsPhotosRequest(sol: sol, page: page)
        return apiClient
        .send(apiRequest:apiRequest)
        .map{($0 as PhotosEntity).photos}
        .map{
            MarsPhotoMapper.transform(from: ($0 as [MarsPhotoEntity]))
        }
    }
    
   
    
   
        
        
    
    
}
