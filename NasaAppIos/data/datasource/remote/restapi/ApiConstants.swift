//
//  ApiConstants.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation

public enum Url: String{
    case BASE = "https://api.nasa.gov/mars-photos/api/"
    case VERSION_1 = "v1/"
    case MARS_PHOTOS = "rovers/curiosity/photos"
}

public enum ErrorMessage: String{
    case NOT_CREATE = "Unable to create URL components"
    case NOT_GET = "Could not get url"
}

public enum HeaderValue : String {
    case APLICATION_JSON = "application/json"
}

public enum HeaderField : String {
    case ACCEPT = "Accept"
}

public enum QueryParams: String {
    case SOL = "sol"
    case PAGE = "page"
    case API_KEY = "api_key"
}

let NASA_KEY = Bundle
    .main.infoDictionary?["Nasa_Key"] as! String



