//
//  CameraDetailsEntity.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
struct CameraDetailsEntity:Codable{
    let id: Int?
    let name: String?
    let rover_id: Int?
    let full_name: String?
    
    init(id: Int?, name:String?, rover_id:Int?, full_name:String?){
        self.id = id
        self.name = name
        self.rover_id = rover_id
        self.full_name = full_name
    }
    
    init(){
        self.init(id: -1, name: "", rover_id: -1, full_name: "")
    }
    
}

