//
//  MarsPhotoEntity.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
struct MarsPhotoEntity:Codable{
    let id: Int?
    let earth_date: String?
    let img_src: String?
    let rover: RoverEntity?
    let camera: CameraDetailsEntity?
}


