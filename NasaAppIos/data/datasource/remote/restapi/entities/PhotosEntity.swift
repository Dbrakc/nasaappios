//
//  PhotosEntity.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation

struct PhotosEntity : Codable{
    let photos: [MarsPhotoEntity]    
}

