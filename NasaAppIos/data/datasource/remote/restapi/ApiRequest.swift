//
//  ApiRequest.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation

public enum RequestType: String {
    case GET
}

protocol APIRequest {
    var method: RequestType { get }
    var path: String { get }
    var parameters: [String : String] { get }
}

extension APIRequest {
    func request(with baseURL: URL) -> URLRequest {
        guard var components = URLComponents(url: baseURL.appendingPathComponent(path), resolvingAgainstBaseURL: false) else {
            fatalError(ErrorMessage.NOT_CREATE.rawValue)
        }
        
        components.queryItems = parameters.map {
            URLQueryItem(name: String($0), value: String($1))
        }
        
        guard let url = components.url else {
            fatalError(ErrorMessage.NOT_GET.rawValue)
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.addValue(HeaderValue.APLICATION_JSON.rawValue, forHTTPHeaderField: HeaderField.ACCEPT.rawValue)
        return request
    }
}
