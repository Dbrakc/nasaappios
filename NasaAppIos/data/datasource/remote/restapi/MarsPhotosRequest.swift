//
//  MarsPhotosRequest.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation

typealias ParametersDict = Dictionary

struct MarsPhotosRequest: APIRequest {
    var method = RequestType.GET
    var path = "\(Url.VERSION_1.rawValue)\(Url.MARS_PHOTOS.rawValue)"
    var parameters: ParametersDict = [QueryParams.API_KEY.rawValue: NASA_KEY]
    
    init(sol: Int = 2, page: Int = 1) {
        parameters[QueryParams.SOL.rawValue] = String(sol)
        parameters[QueryParams.PAGE.rawValue] = String(page)
    }
}
