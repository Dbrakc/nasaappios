//
//  MarsPhotoMapper.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
struct MarsPhotoMapper : Mapper{
    
    typealias ElementFrom = MarsPhotoEntity
    
    typealias ElementTo = MarsPhoto
    
    static func transform(from entity: MarsPhotoEntity) -> MarsPhoto {
        
        let date = parseDate(from: entity.earth_date ?? "" )
        return MarsPhoto(
            withId: entity.id ?? -1,
            date: date,
            imageUrl: entity.img_src?.replacingOccurrences(of: "http", with: "https") ?? "",
            rover: entity.rover?.name ?? "",
            cameraDetails: CameraDetailsMapper.transform(from: entity.camera ?? CameraDetailsEntity()) 
        )
        
    }
    
    static func transform(from entityList: [MarsPhotoEntity]) -> [MarsPhoto] {
       return entityList.map{transform(from: $0)}
    }
    
    private static func parseDate (from dateString: String ) -> MarsDate {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        let date = dateFormatter.date(from: dateString)
        return date ?? MarsDate(timeIntervalSince1970: 0)
    }
    
}
