//
//  CameraDetailsMapper.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
struct CameraDetailsMapper: Mapper {
    
    typealias ElementFrom = CameraDetailsEntity
    
    typealias ElementTo = CameraDetails
    
    static func transform(from entity: CameraDetailsEntity) -> CameraDetails {
        return CameraDetails(
            withId: entity.id ?? -1,
            name: entity.name ?? "",
            roverId: entity.rover_id ?? -1,
            fullName: entity.full_name ?? "")
    }
    
    static func transform(from entityList: [CameraDetailsEntity]) -> [CameraDetails] {
        return entityList.map{transform(from: $0)}
    }
}
