//
//  MarsPhotoRepoImp.swift
//  NasaAppIos
//
//  Created by David Braga  on 18/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
import RxSwift

struct MarsPhotoRepoImp: MarsPhotoRepo {
    var mockData: [MarsPhoto]?
    
    var dataSource: MarsPhotoDataSource?
    
    init(with dataSource: MarsPhotoDataSource){
        self.dataSource = dataSource
    }
    
    func getMarsPhoto(sol: Int = 2, page: Int=1) -> Observable<[MarsPhoto]> {
        return dataSource?.getMarsPhoto(sol:sol, page:page) ?? Observable<[MarsPhoto]>.empty()
    }
    
}
