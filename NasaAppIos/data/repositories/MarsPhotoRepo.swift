//
//  MarsPhotoRepo.swift
//  NasaAppIos
//
//  Created by David Braga  on 18/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
import RxSwift

protocol MarsPhotoRepo{
    var mockData: [MarsPhoto]? {get}
    func getMarsPhoto(sol: Int,page: Int) -> Observable<[MarsPhoto]>
}
