//
//  Photo.swift
//  NasaAppIos
//
//  Created by David Braga  on 17/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation


typealias MarsPhoto = Photo
typealias MarsDate = Date

struct Photo {
    
    let id : Int
    let date: MarsDate
    var imageUrl: String 
    let rover : String
    let cameraDetails: CameraDetails
    
    
    init(withId id: Int, date: MarsDate, imageUrl: String, rover: String, cameraDetails: CameraDetails) {
        self.id = id
        self.date = date
        self.imageUrl = imageUrl
        self.rover = rover
        self.cameraDetails = cameraDetails
    }
    
    func transformDateString(from dateString: MarsDate) -> Date{
        return Date()
    }
}

extension Photo: Equatable {
    var proxyForCompare: Int {
        return id
    }
    
    static func == (lhs: Photo, rhs: Photo) -> Bool {
        return lhs.proxyForCompare == rhs.proxyForCompare
    }
}

extension Photo: Hashable{
    var hashValue: Int {
        return id.hashValue
    }
}









