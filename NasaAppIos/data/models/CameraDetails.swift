//
//  CameraDetails.swift
//  NasaAppIos
//
//  Created by David Braga  on 17/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation

struct CameraDetails{
    let id: Int
    let name: String
    let roverId: Int
    let fullName: String
    
    init(withId id: Int, name: String, roverId: Int, fullName: String) {
        self.id = id
        self.name = name
        self.roverId = roverId
        self.fullName = fullName
    }
    
    init(withId id: Int){
        self.init(withId: id, name: "", roverId: -1, fullName: "")
    }
}



