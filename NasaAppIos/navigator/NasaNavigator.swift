//
//  NasaNavigator.swift
//  NasaAppIos
//
//  Created by David Braga  on 21/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import UIKit
import Swinject

struct NasaNavigator {
   
    let container: Container
    
    init(with container: Container){
        self.container = container
    }
    
    
    func navigateToMarsDetails(marsPhoto: MarsPhoto, navigationController: UINavigationController?){
        registerMarsDetailsContiner(marsPhoto: marsPhoto)
        let marsDestailsVC = container.resolve(MarsDetailsViewController.self)
        navigationController?.pushViewController(marsDestailsVC!, animated: true)
    }
    
}


extension NasaNavigator{
    fileprivate func registerMarsDetailsContiner(marsPhoto: MarsPhoto) {
        container.register(MarsDetailsViewController.self){ _ in
            let controller = MarsDetailsViewController()
            controller.marsPhoto = marsPhoto
            return controller
        }
    }
    
}



