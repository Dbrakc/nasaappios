//
//  containers.swift
//  NasaAppIos
//
//  Created by David Braga  on 18/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
import Swinject

extension AppDelegate {
    
    func registerContainers(){
        registerDataSource()
        registerRepo()
        registerMarsListViewModel()
        registerMarsListViewController()
    }
    
    func registerDataSource(){
        container.register(MarsPhotoDataSource.self) {_ in
            RemoteDataSource()
        }
    }
    
    func registerRepo(){
        container.register(MarsPhotoRepo.self) {
            MarsPhotoRepoImp(with: $0.resolve(MarsPhotoDataSource.self)!)
        }
    }
    
    
    func registerMarsListViewModel(){
        container.register(MarsListViewModel.self) {
            MarsListViewModelImp(repo: $0.resolve(MarsPhotoRepo.self)!,router: self.router!)
        }
    }
    
    func registerMarsListViewController(){
         container.register(MarsListViewController.self){
            let controller = MarsListViewController()
            controller.viewModel = $0.resolve(MarsListViewModel.self)
            return controller
            
        }
    }

}

