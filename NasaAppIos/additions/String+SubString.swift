//
//  String+SubString.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
extension String{
    func substring (from start: Int, to end: Int)->String{
        let start = self.index(self.startIndex, offsetBy: start)
        let end = self.index(self.startIndex, offsetBy: end)
        let range = start..<end
        return String(self[range])
    }
}
