//
//  UIImageView+LoadImage.swift
//  NasaAppIos
//
//  Created by David Braga  on 20/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import UIKit

extension UIImageView{
    func loadImage (with urlString: String){
        let optUrl = URL(string: urlString)
        guard let url = optUrl else {
            return
        }
        let optData = try? Data(contentsOf: url)
        guard let data = optData else{
            return
        }
        self.image = UIImage(data: data)
        
    }
    
}

