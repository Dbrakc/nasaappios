//
//  UiIViewController+navigation.swift
//  NasaAppIos
//
//  Created by David Braga  on 19/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func wrappedInNavigation() -> UINavigationController {
        return UINavigationController(rootViewController: self)
    }
}
