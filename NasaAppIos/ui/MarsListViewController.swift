//
//  MarsListViewController.swift
//  NasaAppIos
//
//  Created by David Braga  on 18/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import UIKit

class MarsListViewController: UIViewController {
    
    // MARK: - Properties
    
    var viewModel : MarsListViewModel?
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Initializers
    init(){
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
