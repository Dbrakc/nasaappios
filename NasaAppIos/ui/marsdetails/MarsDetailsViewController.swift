//
//  MarsDetailsViewController.swift
//  NasaAppIos
//
//  Created by David Braga  on 21/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import UIKit

class MarsDetailsViewController: UIViewController {
    
    // MARK:  - Properties
    
    public var marsPhoto: MarsPhoto?
    
    
    // MARK:  - IBOutlets

    @IBOutlet weak var posterView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var fullNameLabel: UILabel!
    
    @IBOutlet weak var roverIdLabel: UILabel!
    
    // MARK:  - LyfeCycle

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title =
            marsPhoto?.rover ?? ""
        self.nameLabel.text =
            marsPhoto?.cameraDetails.name ?? ""
        self.fullNameLabel.text = marsPhoto?.cameraDetails.fullName ?? ""
        self.roverIdLabel.text = marsPhoto?.cameraDetails.id.description ?? ""
        guard let imageUrl = marsPhoto?.imageUrl else {return}
        self.posterView.loadImage(with: imageUrl)
    }

}
