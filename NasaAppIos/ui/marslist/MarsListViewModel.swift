//
//  MarsListViewModel.swift
//  NasaAppIos
//
//  Created by David Braga  on 18/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
import RxSwift
protocol MarsListViewModel {
   
    var loadSubject: PublishSubject<[MarsPhoto]> {get}
    var marsPhotos: [MarsPhoto] {get}
    var router: NasaNavigator {get}
    
    func loadMarsPhoto(sol: Int)
    func updateMarsPhotos(sol:Int, page:Int)
    func getMarsPhotos()->[MarsPhoto]
}
