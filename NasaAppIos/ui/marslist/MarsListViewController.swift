//
//  MarsListViewController.swift
//  NasaAppIos
//
//  Created by David Braga  on 18/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import UIKit
import RxSwift

class MarsListViewController: UIViewController {
    
    // MARK: - Properties
    let TITLE = "Photos of mars"
    var viewModel : MarsListViewModel?
    internal var marsPhotos: [MarsPhoto] = []
    private var disposeBag: DisposeBag?
    private var page = 1;

    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    // MARK: - Initializers
    init(){
        super.init(nibName: nil, bundle: nil)
        self.title = TITLE
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        disposeBag = DisposeBag()
        setUpTableView()
        
    }
    
    // MARK:  - Private Methods

    private func setUpTableView(){
        viewModel?
            .loadSubject
            .subscribe(onNext: { marsPhotos in
                print(self.viewModel!.marsPhotos.count)
                if(self.viewModel!.getMarsPhotos().count  <= 25){
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.tableView.tableFooterView = UIView()
                    self.tableView.rowHeight = UITableView.automaticDimension
                    self.tableView.estimatedRowHeight = 40
                    let nib = UINib(nibName: MarsPhotoCell.nibName, bundle: nil)
                    self.tableView.register(nib, forCellReuseIdentifier: MarsPhotoCell.reuseIdentifierCell )
                    self.spinner.stopAnimating()
                }else{
                    self.tableView.reloadData()
                }
            }).disposed(by: disposeBag!)
        
    }
    
    
    private func hideProgress() {
        spinner.stopAnimating()
    }
}

extension MarsListViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 223.5
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let optMarsPhoto = self.viewModel?.getMarsPhotos()[indexPath.row]
        guard let marsPhoto = optMarsPhoto else {return}
        self.viewModel?.router.navigateToMarsDetails(marsPhoto: marsPhoto, navigationController: self.navigationController )
        }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height)
        {
            if !(spinner.isAnimating){
                self.page=self.page+1
                print(page)
                self.viewModel?.updateMarsPhotos(sol: 2, page: page)
            }
        }
        
        
    }
    
}

extension MarsListViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.getMarsPhotos().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = MarsPhotoCell.reuseIdentifierCell
        let marsPhoto = self.viewModel?.getMarsPhotos()[indexPath.row]
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId ) as? MarsPhotoCell
        if cell == nil {
            cell = MarsPhotoCell(style: .default, reuseIdentifier: cellId)
        }
        guard let date = marsPhoto?.date else { return cell! }
        cell?.setUpCell(urlString: marsPhoto?.imageUrl ?? "", date: date)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
}
    






