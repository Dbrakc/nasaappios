//
//  MarsListViewModelImp.swift
//  NasaAppIos
//
//  Created by David Braga  on 18/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import Foundation
import RxSwift

class MarsListViewModelImp: MarsListViewModel {
    
    
    
    
    // MARK: - Properties
    
    private var repo : MarsPhotoRepo
    var loadSubject : PublishSubject = PublishSubject<[MarsPhoto]>()
    private let disposeBag: DisposeBag = DisposeBag()
    var router: NasaNavigator
    internal var marsPhotos: [MarsPhoto] = []
    
    
    // MARK: - Initializers
    
    init(repo: MarsPhotoRepo, router: NasaNavigator){
        self.repo = repo
        self.router = router
        loadMarsPhoto()
    }
    
    
    // MARK: - Methods
    
    func loadMarsPhoto(sol: Int = 2) {
        repo
            .getMarsPhoto(sol: sol, page: 1)
            .subscribeOn(SerialDispatchQueueScheduler(qos:.background))
            .observeOn(MainScheduler.instance)
            .subscribe(onNext:
                { [weak self] newPhotos in
                    self?.marsPhotos.removeAll()
                    self?.marsPhotos.append(contentsOf: newPhotos)
                    self?.loadSubject.onNext(newPhotos)
            },onError:
            { error in
                self.loadSubject.onError(error)
            })
            .disposed(by: disposeBag)
    }
    
    func updateMarsPhotos(sol: Int, page: Int) {
        repo
            .getMarsPhoto(sol: sol, page: page)
            .subscribeOn(SerialDispatchQueueScheduler(qos:.background))
            .observeOn(MainScheduler.instance)
            .subscribe(onNext:
                { [weak self] newPhotos in
                    self?.marsPhotos.append(contentsOf: newPhotos)
                    self?.loadSubject.onNext(newPhotos)
                },onError:
                { error in
                    self.loadSubject.onError(error)
            })
            .disposed(by: disposeBag)
    }

    
    func getMarsPhotos()->[MarsPhoto]{
        return marsPhotos
    }
 
    
        
}

