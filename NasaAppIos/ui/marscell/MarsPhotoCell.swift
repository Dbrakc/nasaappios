//
//  MarsPhotoCell.swift
//  NasaAppIos
//
//  Created by David Braga  on 19/03/2019.
//  Copyright © 2019 David Braga . All rights reserved.
//

import UIKit

class MarsPhotoCell: UITableViewCell {
    // MARK: - Properties
    public static let reuseIdentifierCell = "MarsPhotoCell"
    public static let nibName = "MarsPhotoCell"
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var posterView: UIImageView!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        posterView.image = nil
        dateLabel.text = nil
    }
    
    // MARK:  - Methods

    func setUpCell(urlString: String, date: Date){
        dateLabel.text = DatePretyffier.prettify(date: date)
        posterView.loadImage(with: urlString)
    }
    
}
